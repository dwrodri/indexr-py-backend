"""
Executable script for generating useful visualizations/insights of the corpus.
"""

import search_runtime  # pylint: disable=import-error

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.decomposition import NMF
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.manifold import TSNE
from typing import List


def print_top_words(model, feature_names, n_top_words):
    for topic_idx, topic in enumerate(model.components_):
        message = "Topic #%d: " % topic_idx
        message += " ".join(
            [feature_names[i] for i in topic.argsort()[: -n_top_words - 1 : -1]]
        )
        print(message)
    print()


if __name__ == "__main__":
    # Section 1: T-SNE of TFIDF with labelled primary categories
    dimension_reducer = TSNE(
        n_components=2,
        perplexity=50,
        early_exaggeration=12.0,
        learning_rate=200,
        n_iter=1000,
        n_iter_without_progress=300,
        min_grad_norm=1e-7,
        metric="cosine",
        init="pca",
        verbose=True,
        random_state=None,
        method="barnes_hut",
        angle=0.5,
        n_jobs=7,
    )
    reduced_data = dimension_reducer.fit_transform(search_runtime.term_mat)
    labels = search_runtime.meta_df[["title", "categories"]]
    labels["categories"] = labels["categories"].apply(lambda n: n[0])
    sns.scatterplot(
        x=reduced_data[:, 0], y=reduced_data[:, 1], hue="categories", data=labels
    )
    plt.show()
