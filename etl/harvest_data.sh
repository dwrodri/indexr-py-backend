#!/usr/bin/env bash

metha-sync -from 2020-06-01 "https://export.arxiv.org/oai2" &&\
    find .methacache -name "*.xml.gz" | parallel gunzip &&\
    find .methacache -name "*.xml" | parallel python3 oai_dc_xml_parser.py &&\
    python3 gather_parquets.py *.parquet
