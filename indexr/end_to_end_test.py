import search_runtime  # pylint: disable=import-error
import backend  # pylint: disable=import-error

import asyncio
from pprint import pprint
from typing import List

test_samples = [
    "2002.02896",  # Beer Tapping (Fluid Dynamics)
    "1706.03762",  # Transformers (NLP)
    "1712.01815",  # AlphaZero (Reinformcement Learning)
    "1901.00099",  # Microglial memory (Cell Behavior)
    "1804.06826",  # Analysis of Volta GPUs (Distributed Computing??)
]  # type: List[str]


async def generate_report(ids: List[str]):
    for paper_id in ids:
        topic_results = await backend.topic_search(paper_id)
        tfidf_results = await backend.topic_search(paper_id)
        meta_data = search_runtime.meta_df[search_runtime.meta_df["id"] == paper_id]
        print(f"Search Results for {meta_data['title']} ({meta_data['id']})")
        print("TOPIC MODEL RESULTS")
        pprint(topic_results, width=100)
        print("TF-IDF RESULTS")
        pprint(tfidf_results, width=100)


if __name__ == "__main__":
    asyncio.run(generate_report(test_samples))

