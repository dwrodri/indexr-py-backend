"""
Contains global state of the recomendation engine and the key search functions
"""

import arxiv
from datetime import datetime
import time
import pandas as pd  # type: ignore
import numpy as np  # type: ignore
import scipy.sparse  # type: ignore
from functools import lru_cache
from joblib import load as load_pickle
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import NMF
from sklearn.preprocessing import normalize
from gensim.models import LdaModel as LDA
from gensim.matutils import Sparse2Corpus
from typing import Union, List, Dict, Any


#######################################
# Setup work at Import-time
######################################

META_DF_FILENAME = "../arXiv_dataset.parquet"
TOPIC_MAT_FILENAME = "../semiprod_doc_topic_matrix2020-08-21T02-12-21.npy"
TERM_MAT_FILENAME = "../term_mats/semiprod_term_mat_2020-08-21T02-12-21.npz"
MODEL_PICKLE_FILENAME = "../pickles/semiprod_lda_2020-08-21T02-12-21"
VECTORIZER_FILE_NAME = "../pickles/semiprod_tfidf_vec_2020-08-21T02-12-21"

# This is the classifier object used for inferring new papers
with open(MODEL_PICKLE_FILENAME, "rb") as fh:
    topic_model = load_pickle(fh)  # type: Union[NMF, LDA]

# This is the object capable of parsing text into a vector of weighted text
# tokens based on the pre-determined corpus vocabulary
with open(VECTORIZER_FILE_NAME, "rb") as fh:
    vectorizer = load_pickle(fh)  # type: TfidfVectorizer


# This is the main in-memory datastore for all paper info
meta_df = pd.read_parquet(META_DF_FILENAME)  # type: pd.DataFrame

# A matrix where all rows correspond to papers, and columns correspond to vocab
doc_term_mat = scipy.sparse.load_npz(TERM_MAT_FILENAME)  # type: scipy.sparse.csr_matrix

# Bit-matrix tracking the categories of each paper. Useful for fast subsetting of the corpus
category_mat = np.matrix(meta_df["category_bits"].tolist())  # type: np.ndarray

# The Topic Matrix that is a reduced projection of the Document-Term matrix.
doc_topic_mat = np.load(TOPIC_MAT_FILENAME)  # type: np.ndarray

# Lookup table used to decode the category bit-matrix.
arXiv_categories = sorted(
    list(meta_df["categories"].explode().unique())
)  # type: List[str]


# Normalize the topic matrix so that we can do fast comparisons
# Cosine of pre-normalized vectors == simple dot product
doc_topic_mat /= np.linalg.norm(doc_topic_mat, axis=0)

# Do the same for the document-term matrix
normalize(doc_term_mat, norm="l2", axis=1)


#######################################
# Private Helper Functions
######################################


def _fetch_metadata_from_api(query_id: str) -> Dict[str, Any]:
    """
    Call the arXiv API and clean the response for our uses.

    TODO: replace the arxiv package with a custom query written using
    grequests.
    """
    arxiv_api_response = arxiv.query(id_list=[query_id])[0]
    return {
        "date": datetime.fromtimestamp(
            time.mktime(arxiv_api_response["published_parsed"])
        ),
        "id": arxiv_api_response["id"],
        "title": arxiv_api_response["title"],
        "authors": arxiv_api_response["authors"],
        "categories": [tag_obj["term"] for tag_obj in arxiv_api_response["tags"]],
        "abstract": arxiv_api_response["summary"],
    }


def _add_new_metadata_to_global_state(query_id: str):
    """
    Generate data for new paper and add it to the global structures.

    Note that these structures are on a per-worker basis, and that 
    """
    global doc_topic_mat, doc_term_mat, category_mat, meta_df, arXiv_categories
    # get paper from arXiv API and parse response
    cleaned_metadata_dict = _fetch_metadata_from_api(query_id)

    # Generate Category bitset and append to existing matrix
    # super shitty fix for the fact that cs.LG is missing
    if "cs.LG" in cleaned_metadata_dict["categories"]:
        cleaned_metadata_dict["categories"][
            cleaned_metadata_dict["categories"].index("cs.LG")
        ] = "stat.ML"
    new_paper_cat_idxs = [
        arXiv_categories.index(category)
        for category in cleaned_metadata_dict["categories"]
    ]
    cleaned_metadata_dict["category_bits"] = np.packbits(
        [(0, 1)[idx in new_paper_cat_idxs] for idx in range(len(arXiv_categories))]
    )
    category_mat = np.vstack((category_mat, cleaned_metadata_dict["category_bits"]))

    # Vectorize abstract and append to doc_term_mat
    term_vector = vectorizer.transform([cleaned_metadata_dict["abstract"]])
    doc_term_mat = scipy.sparse.vstack([doc_term_mat, term_vector])

    # Get topic vector from term_vector and append to doc_topic_mat
    if isinstance(topic_model, NMF):
        pass
    elif isinstance(topic_model, LDA):
        gensim_doc_wrapper = Sparse2Corpus(term_vector, documents_columns=False)
        topic_vector = np.array([x[1] for x in topic_model[gensim_doc_wrapper[0]]])
        # topic_model.update(gensim_doc_wrapper)  # yay, online learning!

    doc_topic_mat = np.vstack((doc_topic_mat, topic_vector))

    meta_df = meta_df.append(cleaned_metadata_dict, ignore_index=True)


def _convert_arXiv_id_to_idx(query_id: str) -> int:
    """
    Takes an arXiv preprint ID and finds the corresponding index in the global structures.

    Note that the indices of all globla data structures are associative, so by querying 
    meta_df, we can use the same number in all the other structures.
    """
    global meta_df
    conversion_result = meta_df[meta_df["id"] == query_id].index  # type: pd.RangeIndex
    if len(conversion_result) == 0:
        _add_new_metadata_to_global_state(query_id)
        return len(meta_df) - 1
    else:
        return conversion_result[0]


def _get_metadata_batch(row_idxs: List[int]) -> pd.DataFrame:
    """
    Return a subset of meta_df containing the requested indices.
    """
    return meta_df.iloc[row_idxs]


#####################################
# Global Functions
####################################


@lru_cache(maxsize=len(meta_df) // 5)
def topic_mat_search(query_id: str, top_n: int = 10) -> pd.DataFrame:
    """
    Given row_idx, return top_n most similar rows using cosine similarity.
    """
    query_idx = _convert_arXiv_id_to_idx(query_id)
    query_cat_vector = meta_df.iloc[query_idx].category_bits
    search_space = [
        idx
        for idx in np.unique((query_cat_vector & category_mat).nonzero()[0])
        if idx != query_idx
    ]
    # Alternatively : ALL Categories must match
    # search_space = [idx for idx in np.where(
    #     (category_mat == query_cat_vector).all(axis=1)
    # )[0] if idx != query_idx]
    best_from_search_space = np.argpartition(
        doc_topic_mat[search_space, :].dot(doc_topic_mat[query_idx]), top_n,
    )[:top_n]
    return _get_metadata_batch(np.take(search_space, best_from_search_space))


@lru_cache(maxsize=len(meta_df) // 5)
def tfidf_search(query_id: str, top_n: int = 10) -> pd.DataFrame:
    """
    Performs search directly on tfidf matrix instead of topic matrix.

    TODO: The two search functions are actually almost identical now, the problem is 
    I want two LRU caches so I will have to refactor this all later.
    """
    query_idx = _convert_arXiv_id_to_idx(query_id)
    query_cat_vector = meta_df.iloc[query_idx]["category_bits"]
    search_space = [
        idx
        for idx in np.unique((query_cat_vector & category_mat).nonzero()[0])
        if idx != query_idx
    ]
    best_from_search_space = np.argpartition(
        doc_term_mat[search_space, :]
        .dot(doc_term_mat[query_idx].T)
        .toarray()
        .flatten(),
        top_n,
    )[:top_n]
    return _get_metadata_batch(np.take(search_space, best_from_search_space))
