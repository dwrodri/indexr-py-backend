# Author: Derek Rodriguez
# Date: June 9 2020
# Notes: Code for converting Dublin Core XML to Parquet. Call this in parallel.

import xmltodict  # type: ignore
import functools
import operator
import pandas as pd
import sys

if __name__ == "__main__":
    df = pd.DataFrame(
        columns=["date", "id", "title", "authors", "categories", "abstract"]
    )
    with open(sys.argv[1], "rb") as fh:
        record = xmltodict.parse(fh)
        for entry in map(
            lambda n: n["metadata"]["oai_dc:dc"],
            record["Response"]["ListRecords"]["record"],
        ):
            cleaned_title = entry["dc:title"]
            cleaned_author = entry["dc:creator"]
            if not isinstance(cleaned_author, list):
                cleaned_author = [cleaned_author]
            cleaned_id = entry["dc:identifier"]
            if isinstance(cleaned_id, list):
                cleaned_id = cleaned_id[0].split("abs/")[-1]
            else:
                cleaned_id = cleaned_id.split("abs/")[-1]
            cleaned_abs = functools.reduce(operator.add, entry["dc:description"]).strip(
                "\n"
            )
            cleaned_date = entry["dc:date"]
            if isinstance(cleaned_date, list):
                cleaned_date = cleaned_date[-1]
            cleaned_cats = entry["dc:subject"]
            if not isinstance(cleaned_cats, list):
                cleaned_cats = [cleaned_cats]
            df = df.append(
                {
                    "date": cleaned_date,
                    "id": cleaned_id,
                    "title": cleaned_title,
                    "authors": cleaned_author,
                    "categories": cleaned_cats,
                    "abstract": cleaned_abs,
                },
                ignore_index=True,
            )
            df.to_parquet(sys.argv[1][:-4] + ".parquet")
