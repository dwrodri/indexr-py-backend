import json
from tqdm import tqdm
import datetime
import pandas as pd
import numpy as np
import sys

tqdm.pandas()
# load first file
df = pd.read_parquet(sys.argv[1])
# collect all the files
print("Combining files...")
for filename in tqdm(sys.argv[2:]):
    df = pd.concat([df, pd.read_parquet(filename)], ignore_index=True)

# use lookup table to get abbreviated papers
with open("abbrev_table.json", "r") as fh:
    abbrev_table = json.load(fh)
    # The first categories seem like the most consistent, so lets just use those
    cat_set = sorted(list(set(df["categories"].apply(lambda x: x[0]).values)))
    # Remove all other category labels from the dataset
    df["categories"] = df["categories"].apply(lambda x: [y for y in x if y in cat_set])

    # preprocessing functions
    def remove_field(label: str) -> str:
        if " - " in label:
            return label.split(" - ")[1]
        else:
            return label

    def safe_lookup(key: str) -> str:
        try:
            return abbrev_table[key]
        except KeyError:
            return key

    # now do the work
    print("Abbreviating Categories...")
    df["categories"] = df["categories"].progress_apply(
        lambda x: list(map(lambda y: safe_lookup(remove_field(y)), x))
    )

    # generate  bit_vectors
    print("Generating optimized category representations...")
    cat_set = sorted(list(set(df["categories"].apply(lambda x: x[0]).values)))
    df["category_bits"] = (
        df["categories"]
        .progress_apply(lambda x: [cat_set.index(y) for y in x])
        .progress_apply(
            lambda x: np.packbits([(0, 1)[i in x] for i in range(len(cat_set))])
        )
    )


# make date strings dattime objs
df["date"] = pd.to_datetime(df["date"])

# Write master parquet
print("Writing files to disk...")
df.to_parquet("oai_arXiv_harvest.parquet")

# generate parquet of papers in last 5 yrs
five_years_ago = datetime.datetime.now() - datetime.timedelta(days=365 * 5)
df[df["date"] >= five_years_ago].reset_index(drop=True).to_parquet(
    "production_arXiv_metadata.parquet"
)
