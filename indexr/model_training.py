"""
Main script for producing new topic matrics and TFIDF matrices.
"""

import joblib  # type: ignore
import pandas as pd
import numpy as np
from os import cpu_count
from datetime import datetime
from argparse import ArgumentParser, Namespace
from gensim.matutils import Sparse2Corpus
from gensim.models.callbacks import PerplexityMetric
from typing import List, Tuple
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import NMF
from gensim.models import LdaModel as LDA
from tqdm import tqdm  # type: ignore
import scipy
from typing import Union


def _vectorize_documents(
    docs: List[str], vocab_size: int, chose_nmf=True
) -> Tuple[scipy.sparse.csr_matrix, TfidfVectorizer]:
    """ 
    Creates doc-term matrix using a vectorizer.

    Returns TF matrix when algo of choice is LDA, otherwise TF-IDF matrix. 
    Vectorization parameters are referenced from Blei's 2009 ML Summer School 
    lecture on LDA. See https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html
    """
    vectorizer = TfidfVectorizer(
        max_df=0.90,
        min_df=2,
        max_features=vocab_size,
        strip_accents="unicode",
        stop_words="english",
        use_idf=chose_nmf,
        smooth_idf=chose_nmf,
        token_pattern=r"(?u)\b[a-zA-Z_][a-zA-Z0-9_]+\b",
    )
    doc_term_mat = vectorizer.fit_transform(docs)
    return (doc_term_mat, vectorizer)


def main(args: Namespace):
    """
    Generates doc-topic matrix and saves to file.

    Also saves intermediate objects if desired.
    """
    # Load preprint dataset and vectorize
    abstracts = pd.read_parquet(args.dataset)["abstract"].tolist()
    print("Vectorizing corpus")
    doc_term_mat, vectorizer = _vectorize_documents(
        abstracts, args.vocab_size, args.algorithm == "nmf"
    )
    # Instantiate NMF object and create doc-topic mat
    if args.load_model == "NEW":
        if args.algorithm == "nmf":
            print("Fitting new NMF model to doc-term matrix.")
            model = NMF(
                n_components=args.n_topics,
                init="nndsvd",
                solver="cd",
                beta_loss="frobenius",
                l1_ratio=0.5,
                verbose=True,
                shuffle=False,
                max_iter=args.n_iter,
                tol=args.tolerance,
            )  # type: Union[NMF, LDA]
            doc_topic_mat = model.fit_transform(doc_term_mat)
        else:
            print("Converting Corpus and fitting Gensim LDA model")
            optimal_cores = cpu_count()
            if optimal_cores is not None:
                optimal_cores = optimal_cores // 2 - 1
                gensim_corpus = Sparse2Corpus(doc_term_mat, documents_columns=False)
                perplexity_logger = PerplexityMetric(
                    corpus=gensim_corpus, logger="shell"
                )
                model = LDA(
                    corpus=gensim_corpus,
                    num_topics=args.n_topics,
                    # workers=optimal_cores,
                    chunksize=5000,
                    alpha="auto",
                    iterations=args.n_iter,
                    minimum_probability=0,
                    callbacks=[perplexity_logger],
                )
                doc_topic_mat = np.zeros((doc_term_mat.shape[0], args.n_topics))
                for i in tqdm(
                    iterable=range(len(gensim_corpus)),
                    desc="Extracting Document-Topic Matrix",
                    unit="doc",
                ):
                    doc_topic_mat[i, :] = np.array(
                        [x[1] for x in model[gensim_corpus[i]]]
                    )
                print(doc_topic_mat.shape)
    else:
        print(f"Loading pre-trained model: {args.load_model}")
        with open(args.load_model) as fh:
            model = joblib.load(args.load_model)
        if args.algorithm == "nmf":
            doc_topic_mat = model.transform(doc_term_mat)
        else:
            model.update(
                corpus=Sparse2Corpus(doc_topic_mat, documents_columns=False),
                chunks_as_numpy=False,
            )

    # save results to files
    timestamp = datetime.now().isoformat(timespec="seconds").replace(":", "-")
    if not args.no_save_matrix:
        print("Saving document_topic_matrix to ../")
        np.save("../" + args.prefix + "_doc_topic_matrix" + timestamp, doc_topic_mat)
    if args.load_model == "NEW":
        print("saving new model to ../pickles/")
        if args.algorithm == "nmf":
            with open("../pickles/" + args.prefix + "_nmf_" + timestamp, "wb") as fh:  # type: ignore
                joblib.dump(model, fh)
        else:
            with open("../pickles/" + args.prefix + "_lda_" + timestamp, "wb") as fh:  # type: ignore
                joblib.dump(model, fh)
    if args.pickle_vectorizer:
        print("saving new CountVectorizer obj to ../pickles/")
        with open("../pickles/" + args.prefix + "_tfidf_vec_" + timestamp, "wb") as fh:  # type: ignore
            joblib.dump(vectorizer, fh)
    if args.save_term_mat:
        print("saving term matrix as npz to ../term_mats/")
        with open(  # type: ignore
            "../term_mats/" + args.prefix + "_term_mat_" + timestamp + ".npz", "wb"
        ) as fh:
            scipy.sparse.save_npz(fh, doc_term_mat)


if __name__ == "__main__":
    parser = ArgumentParser(
        "Script for Generating Doc-Topic Matrix and intermediate model."
    )
    parser.add_argument(
        "--vocab_size",
        action="store",
        type=int,
        default=10000,
        help="Amt. of words to track in corpus",
    )
    parser.add_argument(
        "--tolerance",
        action="store",
        type=float,
        default=0.1,
        help="Training will stop after the model-specific metric decreases by less than this number.",
    )
    parser.add_argument(
        "--n_iter",
        action="store",
        type=int,
        default=1000,
        help="Max number of iterations for the training algorithm.",
    )
    parser.add_argument(
        "--n_topics",
        action="store",
        type=int,
        default=300,
        help="Number topics learned in matrix",
    )
    parser.add_argument(
        "--dataset",
        action="store",
        type=str,
        default="../arXiv_dataset.parquet",
        help="Location of parquet containing document metadata.",
    )
    parser.add_argument(
        "--prefix",
        action="store",
        type=str,
        default="",
        help="prefix to attach to filenames",
    )
    parser.add_argument(
        "--algorithm",
        choices=["nmf", "lda"],
        default="nmf",
        help="Topic modelling algo to use.",
    )
    parser.add_argument(
        "--load_model",
        action="store",
        type=str,
        default="NEW",
        help="Name of pickled pre-trained model. If not set, generates new model",
    )
    parser.add_argument(
        "--pickle_vectorizer",
        action="store_true",
        help="Pickle Vectorizer and save to a file.",
    )
    parser.add_argument(
        "--no_save_matrix",
        action="store_true",
        help="Disable writing the matrix to a file.",
    )
    parser.add_argument(
        "--save_term_mat", action="store_true", help="Save TF(IDF) matrix to a file"
    )
    main(parser.parse_args())
