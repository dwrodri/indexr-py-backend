"""
Specification of FastAPI endpoints. This is a wrapper that heavily interfaces 
with the search_runtime module, where most of the actual number crunching and
data work is actually done.

In the future, search_runtime will deprecate direct access to global objects 
in favor of a cleaner custom interface.
"""
import search_runtime  # pylint: disable=import-error
import pandas as pd
from fastapi import FastAPI, Query
from fastapi.middleware.cors import CORSMiddleware

from argparse import ArgumentParser
from functools import lru_cache
from typing import Union, Dict, List, Tuple, Iterable

backend = FastAPI()
backend.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@backend.get("/meta/{query_id}")
async def get_meta(query_id: str = Query(None, min_length=9, max_length=16)):
    """
    Return metadata as stored in the backend's local DataFrame.

    :param query_id: arXiv ID of the paper being queried
    :returns: row from paper DataFrame as a dictionary
    ::todo Add a regex for validating IDs
    """
    return {
        "Paper": {
            k: repr(v[0])
            for k, v in search_runtime.meta_df[search_runtime.meta_df["id"] == query_id]
            .to_dict("list")
            .items()
        }
    }


@backend.get("/search/topic/{query_id}")
async def topic_search(
    query_id: str = Query(None, min_length=9, max_length=16)
) -> Dict[str, List[Dict[str, str]]]:
    """
    Use a precomputed document-topic matrix to query similar papers.

    :param query_id: The arXiv ID of the paper being queried
    :returns: List of topically similar papers
    ::todo allow handling of unknown IDs
    """
    reply_json = {"results": []}  # type: Dict[str, List[Dict[str, str]]]
    for paper in search_runtime.topic_mat_search(query_id, top_n=10).itertuples():
        reply_json["results"].append(
            {
                "title": paper.title,
                "authors": paper.authors.tolist(),
                "categories": paper.categories.tolist(),
                "abstract": paper.abstract,
                "url": "https://arxiv.org/abs/" + paper.id,
            }
        )
    return reply_json


@backend.get("/search/tfidf/{query_id}")
async def tfidf_search(
    query_id: str = Query(None, min_length=9, max_length=16)
) -> Dict[str, List[Dict[str, str]]]:
    """
    Compare TF-IDF vectors in corpus to a key vector and return the nearest.
    """

    reply_json = {"results": []}  # type: Dict[str, List[Dict[str, str]]]
    for paper in search_runtime.tfidf_search(query_id, top_n=10).itertuples():
        reply_json["results"].append(
            {
                "title": paper.title,
                "authors": paper.authors.tolist(),
                "categories": paper.categories.tolist(),
                "abstract": paper.abstract,
                "url": "https://arxiv.org/abs/" + paper.id,
            }
        )
    return reply_json
